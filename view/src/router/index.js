import Vue from 'vue'
import Router from 'vue-router'

import main from "../view/main/main";
import register from "../view/register/register";
import login from "../view/login/login";
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'mian',
      component: main
    },
    {
      path: '/login',
      name: 'login',
      component: login
    },
    {
      path: '/register',
      name: 'register',
      component: register
    }
  ],
  mode: "history"
})
