# 随聊

#### 介绍
第二次Java大作业作品，具备一下功能：用户登录注册，在线聊天功能。
基于SpringBoot整合SpringMvc Websocket实现注册登录于在线聊天功能，使用JWT做token安全验证。利用Mybatis实现信息存储。前端使用Vue 2.x + Websocket axiso与ElementUI 实现页面展示与在线聊天。

#### 软件架构
后端基于SpringBoot websocket mvc mybatis jwt
前端基于 vue2.x elementUI axios websocket


#### 安装教程

克隆下载源码即可

#### 使用说明

前端：
npm run dev
后端：
运行application即可，端口设置8000

#### SQL环境
使用MYSQL5.7进行搭建。一共创建四个表，目前版本只使用到三个表。
![输入图片说明](image.png)

```

create table friend
(
    fid      bigint auto_increment,
    myid     bigint not null,
    friendid bigint not null,
    constraint friends_fid_uindex
        unique (fid)
)
  

alter table friend
    add primary key (fid);


create table message
(
    mid     bigint auto_increment,
    message text   not null,
    talkid  bigint not null,
    constraint message_mid_uindex
        unique (mid)
)
    comment '这个是用来存对话的';

alter table message
    add primary key (mid);



create table talk
(
    tid     bigint auto_increment,
    mytalk  bigint not null comment '这个是当前用户的id',
    shetalk bigint not null comment '这个是被对话的对象的id',
    constraint talk_tid_uindex
        unique (tid)
)
    comment '这个是用存储用户交流的表，每个人对应的话都要分开了存储，方便后面加入撤回功能';

alter table talk
    add primary key (tid);

create table user
(
    id       bigint auto_increment,
    account  varchar(128) not null,
    username varchar(24)  not null,
    password varchar(128) not null,
    constraint user_account_uindex
        unique (account),
    constraint user_id_uindex
        unique (id)
)
    comment '用户表';

alter table user
    add primary key (id);



```


