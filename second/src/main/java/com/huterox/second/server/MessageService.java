package com.huterox.second.server;

import com.huterox.second.dao.mapper.MessageMapper;
import com.huterox.second.dao.pojo.Message;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MessageService {

    @Autowired
    MessageMapper messageMapper;
    public Long SaveMessage(String message,Long talkid){
        return messageMapper.addMessage(message, talkid);
    }
    public List<Message> getMessagesByTalkID(Long talkid){
        return messageMapper.getMessagesByTalkID(talkid);
    };
}
