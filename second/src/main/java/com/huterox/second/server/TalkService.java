package com.huterox.second.server;

import com.huterox.second.dao.mapper.TalkMapper;
import com.huterox.second.dao.pojo.Talk;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TalkService {

    @Autowired
    TalkMapper talkMapper;
    public Long getTalkId(Long mytalk,Long shetalk){
        Talk talk = talkMapper.findTalk(mytalk, shetalk);
        if(talk!=null){
            return talk.getTid();
        }else {
//            由于我们的主键不是账号，所以没法指定，只能重新查询
            talkMapper.addTalk(mytalk,shetalk);
            talk = talkMapper.findTalk(mytalk, shetalk);
            return talk.getTid();
        }
    }
}
