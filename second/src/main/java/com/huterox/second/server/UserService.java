package com.huterox.second.server;


import com.huterox.second.dao.mapper.UserMapper;
import com.huterox.second.dao.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserService {

    @Autowired
    UserMapper userMapper;
    public User selectUserById(Integer id){return userMapper.selectUserById(id);}
    public User selectUserByAccount(String account){
        return userMapper.selectUserByAccount(account);
    }
    public User selectUserByAccountAndPassword(String account,String password){
        return userMapper.selectUserByAccountAndPassword(account,password);
    }
    @Transactional(rollbackFor = Exception.class)
    public int addUser(String account,String username,String password) throws Exception {
//        发生异常回滚
        int flag = 1;

        try {
            userMapper.AddUser(account, username, password);
        }catch (Exception e){
            flag = -1;
            throw new Exception("用户添加异常");
        }
        return flag;
    }
}
