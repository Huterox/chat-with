package com.huterox.second.config;


import com.alibaba.fastjson.JSON;
import com.huterox.second.dao.message.TokenInMessage;
import com.huterox.second.server.UserService;
import com.huterox.second.utils.TokenProccessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@Component
public class TokenInterceptor implements HandlerInterceptor {

    TokenProccessor tokenProccessor;

    public TokenInterceptor(TokenProccessor tokenProccessor) {
        this.tokenProccessor = tokenProccessor;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response,Object handler)throws Exception{
        if(request.getMethod().equals("OPTIONS")){
            response.setStatus(HttpServletResponse.SC_OK);
            return true;
        }
        response.setCharacterEncoding("utf-8");
        String token = request.getHeader("token");
        int result = 0;
        if(token != null){
            result = tokenProccessor.verify(token);
            if(result == 1){
//                System.out.println("通过拦截器");
                return true;
            }

        }
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json; charset=utf-8");
        try{

            TokenInMessage tokenInMessage = new TokenInMessage();
            tokenInMessage.setSuccess(result);//0表示验证失败,2表示过期
            response.getWriter().append(JSON.toJSONString(tokenInMessage));
//            System.out.println("认证失败，未通过拦截器");
        }catch (Exception e){
            e.printStackTrace();
            response.sendError(500);
            return false;
        }
        return false;
    }
}
