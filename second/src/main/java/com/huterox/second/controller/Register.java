package com.huterox.second.controller;

import com.huterox.second.dao.message.RegisterMessage;
import com.huterox.second.server.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

@Controller
@ResponseBody
public class Register {

    @Autowired
    RegisterMessage registerMessage;
    @Autowired
    UserService userService;

    @PostMapping("/register")
    public RegisterMessage Register(@RequestBody Map<String, Object> userMap) throws Exception {
        String account = (String) userMap.get("account");
        String username = (String)userMap.get("username");
        String password = (String) userMap.get("password");
        if(account!=null && password!=null){
            userService.addUser(account,username,password);
            registerMessage.setFlag(1);
        }else {
            registerMessage.setFlag(-1);
        }
        return registerMessage;
    }
}
