package com.huterox.second.controller;


import com.huterox.second.dao.message.ResultMessage;
import com.huterox.second.server.TalkService;
import com.huterox.second.utils.LoadMessageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

@Controller
@ResponseBody
public class LoadMessage {


    @PostMapping("/loadmessage")
    public List<ResultMessage> loadMessage(@RequestBody Map<String,Object> params){
        Long currentId = Long.parseLong(String.valueOf(params.get("currentId")));
        String currentName = (String) params.get("currentName");
        Long selectID = Long.parseLong(String.valueOf(params.get("selectID")));
//        这个username是被选中的用户名
        String userName = (String) params.get("userName");
        return LoadMessageUtils.LoadMessages(currentId,selectID,currentName,userName);
    }

}
