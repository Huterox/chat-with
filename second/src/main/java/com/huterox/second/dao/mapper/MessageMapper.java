package com.huterox.second.dao.mapper;

import com.huterox.second.dao.pojo.Message;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface MessageMapper {
    List<Message> getAllMessages();
    Long addMessage(@Param("message") String message,@Param("talkid") Long talkid);
    List<Message> getMessagesByTalkID(@Param("talkid") Long talkid);
}
