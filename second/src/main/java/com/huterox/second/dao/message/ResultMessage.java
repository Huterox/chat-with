package com.huterox.second.dao.message;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResultMessage {

    private boolean isSystem;
    private Long fromID;
    private String fromName;
//    这里未来给个拓展，实际上我们本次使用的这个Message其实就是Message对象
    private Object Message;

}
