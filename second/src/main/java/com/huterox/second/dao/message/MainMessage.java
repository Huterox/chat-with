package com.huterox.second.dao.message;


import lombok.Data;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Data
@Component
@Scope("prototype")
public class MainMessage {

    private int success;
    private Long currentId;
    private String currentName;

}
