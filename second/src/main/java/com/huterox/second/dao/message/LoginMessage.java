package com.huterox.second.dao.message;

import lombok.Data;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Data
@Component
@Scope("prototype")
public class LoginMessage {
    private int success;
    private String token;
}
