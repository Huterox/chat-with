package com.huterox.second.dao.pojo;

import lombok.Data;

import lombok.ToString;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

@ToString
@Data
@Repository
@Scope("prototype")
public class Friend {
    private Long fid;
    private Long myid;
    private Long friendid;
}
