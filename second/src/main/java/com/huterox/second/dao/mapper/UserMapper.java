package com.huterox.second.dao.mapper;

import com.huterox.second.dao.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;


import java.util.List;

@Mapper
public interface UserMapper {
    List<User> getAllUsers();
    User selectUserById(@Param("id") Integer id);
    User selectUserByAccount(@Param("account") String account);
    User selectUserByAccountAndPassword(@Param("account") String account,@Param("password") String password);
    Long AddUser(@Param("account") String account,@Param("username") String username,@Param("password") String password);

}
