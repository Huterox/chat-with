package com.huterox.second.dao.pojo;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

@ToString
@Data
@Repository
@Scope("prototype")
public class Message {
    private Long mid;
    private String message;
    private Long talkid;
}
