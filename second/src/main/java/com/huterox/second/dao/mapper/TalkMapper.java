package com.huterox.second.dao.mapper;

import com.huterox.second.dao.pojo.Talk;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface TalkMapper {
    List<Talk> getAllTalks();
    Long addTalk(@Param("mytalk") Long mytalk, @Param("shetalk") Long shetalk);
    Talk findTalk(@Param("mytalk") Long mytalk, @Param("shetalk") Long shetalk);
}
