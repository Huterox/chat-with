package com.huterox.second.dao.message;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class MessageSend {
    private Long toID;
    private String toName;
    private String message;
}
