package com.huterox.second.dao.mapper;

import com.huterox.second.dao.pojo.Friend;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface FriendMapper {

    List<Friend> getAllFriends();

}
